﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ciklus
{
   
    class Program
    {

        public delegate bool div_by_5(int i);



        static void Main(string[] args)
        {

            Random rnd = new Random();

            List<int> rnd_nums = new List<int>(20);

            div_by_5 can_div_by_5 = (i) => { return i % 5 == 0; };

            for (var i = 0; i < rnd_nums.Capacity; i++)
            {
                var rnd_int = rnd.Next(0, 51);
           
                rnd_nums.Add(rnd_int);

                Console.WriteLine($"{rnd_nums.ElementAt(i)}");
            }

            for(var i = 0; i < 15; i++)
            {
                if (can_div_by_5(i))
                {
                    Console.WriteLine(i);
                }
            }

            Console.ReadKey();

        }
    }
}
